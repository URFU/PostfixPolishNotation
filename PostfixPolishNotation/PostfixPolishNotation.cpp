// PostfixPolishNotation.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"

#include <iostream>
#include <stack>
#include <string>
#include <sstream>

using namespace std;
double RPNcalc(string inputString);
string CutString(string &in);
string RPNConvert(string inputString);
void ClearSpace(string &str);
int CheckPriority(char operation);
string CutStackToString(stack <char> &dataStack, int OldPriority);
string ClosedBracked(stack <char> &dataStack);
void ClearBracked(string &str);
void AddSpace(string &str);
void Test(void);

int main() {
	Test();
	string input;
	cout << "Enter an expression that should count. \nInput Example : 5 + 10 * (8 - 4):" << endl;
	getline(cin, input);
	input = RPNConvert(input);
	cout << "Conver RPN: " << input << endl;
	cout << "Answer: " << RPNcalc(input) << endl;
	system("pause");
}

string RPNConvert(string inputString) {
	ClearSpace(inputString);
	string outString = "";
	stack <char> dataStack;
	for (uint32_t i = 0; i < inputString.length(); i++) {
		/*���� ����� - ������ ��������� ��� � �������� ������*/
		if (isdigit(inputString[i])) {
			outString += inputString[i];
		}
		else{
			/*����� �� ���� ������� � ������*/
			if ((outString.length() > 0) && (inputString[i] != '(') && (inputString[i] != ')')) {
				AddSpace(outString);
			}
			/*���� ���� ������ ��� ������ ��� ������ ������ �����- ������ � ����*/
			if (dataStack.empty() || (dataStack.top() == '(') || (inputString[i] == '(')) {
				dataStack.push(inputString[i]);
				AddSpace(outString);
				continue;
			}
			/*����� �������� ������? �������� ��� �� ����� � �������� ������ �� �������� ������*/
			if (inputString[i] == ')') {
				AddSpace(outString);
				outString += ClosedBracked(dataStack);
				continue;
			}
			int oldPriority = CheckPriority(dataStack.top());
			/*��������� �������� � ����� ����, ��� � �����*/
			if (oldPriority < CheckPriority(inputString[i])) {
				dataStack.push(inputString[i]);
				continue;
			} 
			/*��������� �������� � ����� ����, ��� � �����*/
			else {
				outString += CutStackToString(dataStack, oldPriority);
				dataStack.push(inputString[i]);
				continue;
			}
		}
	}
	/*������� ��� ��� �������� �� �����*/
	while (!dataStack.empty()) {
		AddSpace(outString);
		outString += dataStack.top();
		dataStack.pop();
	}
	ClearBracked(outString);
	return outString + " =";
}

void AddSpace(string &str) {
	if (str.length() > 0) {
		if (str[str.length() - 1] != ' ') {
			str += " ";
		}
	}
}

string CutStackToString(stack <char> &dataStack, int OldPriority) {
	string stringOut = "";
	char operation = ' ';
	while (!dataStack.empty() && (operation != '(') && (CheckPriority(dataStack.top()) >= OldPriority)) {
		stringOut += dataStack.top();
		stringOut += " ";
		dataStack.pop();
	}
	return stringOut;
}

string ClosedBracked(stack <char> &dataStack) {
	string stringOut = "";
	while (!dataStack.empty() && (dataStack.top() != '(')) {
		stringOut += dataStack.top();
		AddSpace(stringOut);
		dataStack.pop();
	}
	dataStack.pop();
	return stringOut;
}



double RPNcalc(string inputString) {
	stack<double> da_stack;
	string in = "";
	uint32_t count = 0;
	while (in != "=") {
		in = CutString(inputString);
		if (in == "+") {
			double a = da_stack.top();
			da_stack.pop();
			double b = da_stack.top();
			da_stack.pop();
			da_stack.push(a + b);
		}
		else if (in == "-") {
			double a = da_stack.top();
			da_stack.pop();
			double b = 0;
			if (!da_stack.empty()) {
				b = da_stack.top();
				da_stack.pop();
			}
			da_stack.push(b - a);

		}
		else if (in == "*") {
			double a = da_stack.top();
			da_stack.pop();
			double b = da_stack.top();
			da_stack.pop();
			da_stack.push(a * b);

		}
		else if (in == "/") {
			double a = da_stack.top();
			da_stack.pop();
			double b = da_stack.top();
			da_stack.pop();
			da_stack.push(b / a);

		}
		else if (in == "=") {
			if (da_stack.size() != 1) {
				cout << "Bad expression" << endl;
			}
			else {
				double tmpAnswer = da_stack.top();
				da_stack.pop();
				return tmpAnswer;
			}
		}
		else {
			double d;
			stringstream ss(in);
			ss >> d;

			da_stack.push(d);
		}
	}
}

string CutString(string &in) {
	string::size_type pos;
	pos = in.find(' ', 0);
	string second = in.substr(0, pos);
	in = in.substr(pos + 1); 
	return second;
}

void ClearSpace(string &str) {
	uint32_t i = 0;
	while ((i = str.find(' ')) != string::npos)
		str.erase(i, 1);
}

void ClearBracked(string &str) {
	uint32_t i = 0;
	while ((i = str.find(')')) != string::npos)
		str.erase(i, 1);
	i = 0;
	while ((i = str.find('(')) != string::npos)
		str.erase(i, 1);
}

int CheckPriority(char operation) {
	switch (operation) {
	case '*': return 2;
	case '/': return 2;
	case '+': return 1;
	case '-': return 1;
	default: return 0;
	}
}

void Test(void) {
	{
		string input = "5*5-(4-2)+16";
		string out = RPNConvert(input);
		if (out != "5 5 * 4 2 - - 16 + =") {
			cout << "Error1" << endl;
		}
		if (RPNcalc(out) != 39) {
			cout << "Error2" << endl;
		}
	}
	{
		string input = "-5-5";
		string out = RPNConvert(input);
		if (out != "5 - 5 - =") {
			cout << "Error1" << endl;
		}
		if (RPNcalc(out) != -10) {
			cout << "Error2" << endl;
		}
	}

}